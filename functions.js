$(function(){
	$('.opcoes .fa-trash').on('click',function(){
		$(this).parent().parent().fadeOut();
		var code = $(this).parent().parent().attr('code');
		$.ajax({
			dataType:'json',
			url:$('base').attr('href')+'ajax.php',
			data:{code:code},
			method:'post',
		})
	})

	$('.add').on('click',function(){
		if($('.form-code input[type=text]').is(':hidden') == true){
			$(this).removeClass('bottom10');
			$(this).removeClass('fa');
			$(this).removeClass('fa-plus');
			$(this).addClass('fas');
			$(this).addClass('fa-arrow-left');
			$('.form-code input[type=text]').fadeIn();
		}else{
			$('.form-code').submit();
			$('.form-code input[type=text]').hide();
			$(this).addClass('bottom10');
			$(this).addClass('fa');
			$(this).addClass('fa-plus');
			$(this).removeClass('fas');
			$(this).removeClass('fa-arrow-left');
		}
	})

	$('.select').click(function(){
		location.href=$('base').attr('href')+'?produto='+$(this).parent().attr('code');
	})


	var situacao = $('.statusbar').attr('situation');

	if(situacao == 'postado'){
		$('.ux1').addClass('eached');
		$('.progresso').animate({'width':'3%'},500);
		$('.progresso p').html('Postado');
	}else if(situacao == 'encaminhado'){
		$('.progresso').animate({'width':'3%'},150,function(){
			$('.ux1').addClass('eached');
			$('.progresso p').html('Postado');
			$('.progresso').animate({'width':'37%'},1000,function(){
				$('.ux2').addClass('eached');
				$('.progresso p').html('A Caminho');
				$('.ux2').addClass('animate__tada');
			});
		});

	}else if(situacao == 'entrega'){
		$('.progresso').animate({'width':'3%'},0,function(){
			$('.ux1').addClass('eached');
			$('.progresso p').html('Postado');
			$('.progresso').animate({'width':'37%'},1000,function(){
				$('.ux2').addClass('eached');
				$('.ux2').addClass('animate__headShake');
				$('.progresso p').html('A Caminho');
				$('.progresso').animate({'width':'70%'},1000,function(){
					$('.ux3').addClass('eached');
					$('.progresso p').html('Saiu para Entrega');
					$('.ux3').addClass('animate__tada');
				});
			});
		});

	}else if(situacao == 'entregue'){
		$('.progresso').animate({'width':'3%'},0,function(){
			$('.ux1').addClass('eached');
			$('.progresso p').html('Postado');
			$('.progresso').animate({'width':'37%'},1000,function(){
				$('.ux2').addClass('eached');
				$('.ux2').addClass('animate__headShake');
				$('.progresso p').html('A Caminho');
				$('.progresso').animate({'width':'70%'},1000,function(){
					$('.ux3').addClass('eached');
					$('.ux3').addClass('animate__headShake');
					$('.progresso p').html('Saiu para Entrega');
					$('.progresso').animate({'width':'100%'},1000,function(){
						$('.ux4').addClass('eached');
						$('.ux4').addClass('animate__tada');
						$('.progresso p').html('Entregue');
				});
				});
			});
		});

	}else{

	}
})