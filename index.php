<!-- 

Olá, me chamo Flaviano Rodrigues e estou enviando este e-mail com o resultado da resolução.


Tecnologias utilizadas: mpdf, phpmailer, baru, composer, php, html5, css3, font awesome, jquery e javascript.


Como não sou familiarizado com o gitlab eu subi o projeto em uma hospedagem que eu possuo para fazer testes, vocês poderão acessar o projeto online acessando esse link: http://flaviano.store/trabalho

Para ver o código eu consegui subir ele no gitlab, basta clicar aqui: 

O processo que eu tive para construir esse sistema foi simples porém muito funcional.

De inicio eu foi procurar um web service que me retornasse todas as informações sobre tal produto, foi uma tarefa complicada pois como o php recentemente atualizou para a versão 8 logo muitos web services estavam dando erro quando eu tentava instalar usando composer.

Após conseguir uma dependência que me retornasse o necessário sobre o rastreamento do produto eu só precisei instalar o phpmailer(ferramenta que ja uso a um bom tempo) e o mpdf(no caso é o que eu costumo usar para gerar pdf's). 

Depois de pensar em como eu faria o projeto eu botei a mão na massa e comecei a estiliza-lo. Optei por seguir as mesmas cores que a logo da empresa e foquei também em deixar a estrutura bem amigável.

Como eu gosto muito de utilizar API e web service e já trabalho integrando disparo de e-mail e sistema com pdf em meus projetos logo a implementação desses foi tranquila. Minha maior dificuldade mesmo foi realmente tentar instalar um web service que retorna os dados do produto que fosse compatível com o php 8.

Fiz o projeto de maneira que quem for utilizar conseguirá visualizar varias situações de vários produtos diferentes ao mesmo tempo. Optei por salvar os códigos usando cookies para que de fato seja um projeto fácil e rápido de começar a usar sem precisar ficar criando base de dados no Mysql ou em outro local, porém caso seja necessário em alguns cliques é possível integrar uma base de dados no sistema.

Eu normalmente faço meus projetos usando a estrutura mvc, porém preferi não a utilizar neste projeto pois da maneira que está estruturado se torna muito fácil de integrar ele em algum sistema já existente.

Para visualizar como ficou o e-mail que será disparado para o cliente basta na tela inicial clicar no ícone do E-mail que será enviado para este e-mail: joao.macedo@elastic.fit

Como o gmail e outros serviços parecidos limitam muito a questão do html nos e-mails eu preferi não por uma barra de progresso no e-mail que for enviar ao cliente como foi sugerido nas instruções do projeto mas caso seja necessário posso por a barra de progresso sem nenhum problema.

Sempre que o e-mail for enviado ao cliente o assunto do e-mail será diferente com base no estado em que o produto se encontra e, também no e-mail será possível visualizar todo o progresso do produto(Caso o cliente deseje também é possível visualizar o mesmo em PDF).

Teve algo que eu não consegui fazer ? Sim, tentei fazer para quando enviar o e-mail o PDF ir anexado porém não consegui fazer isso de forma dinâmica. porém para não ficar sem nada, como solução foi colocado uma opção aonde o cliente poderá clicar e baixar o PDF através do e-mail recebido.  -->

<?php 
    define('INCLUDE_PATH','http://flaviano.store/trabalho/');
    // define('INCLUDE_PATH','http://localhost/Trabalho/');
    ob_start();

    require 'vendor/autoload.php';
 ?>
<!DOCTYPE html>
<html>
<head>
    <title>E-lastic Brasil - Envios</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE-Edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <link rel="shortcut icon" href="<?php echo INCLUDE_PATH ?>icon.png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.0.0/animate.min.css">
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@100;300;400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="<?php echo INCLUDE_PATH?>style.css">
</head>
<body>
<base href="<?php echo INCLUDE_PATH ?>">
<header>
   <div class="logo">
       <a href="<?php echo INCLUDE_PATH ?>"><img src="<?php echo INCLUDE_PATH ?>logo.png"></a>
   </div>
   <div class="titulo">
       <p>Envio & Objetos</p>
   </div>
   <div class="clear"></div>
</header>
<div class="container">
<?php 
    if(!isset($_GET['produto'])){

        if(isset($_POST['codigo'])){
            if($_POST['codigo'] != ''){
                setcookie($_POST['codigo'],'true',time()+60*60*24*365,'/');
                header('Location: '.INCLUDE_PATH);
            }
        }
 ?>
 <i class="fa fa-plus bottom10 add"></i>
    <form class="form-code" method="post">
        <input maxlength="13" minlength="13" type="text" name="codigo" placeholder="OA016913717BR">
    </form>
<div class="clear"></div>
<div class="wraper-table">
    <table>
        <tr class="first">
            <td>Status:</td>
            <td>Situação:</td>
            <td>Local:</td>
            <td>Data:</td>
            <td>Opções:</td>
        </tr>
        <?php 
            foreach (array_reverse($_COOKIE) as $key => $value) {
                if(strlen($key) != 13) continue;
                $Correios = new \Baru\Correios\RastreioParser();
                $Correios->setCode($key);
                $Evento = $Correios->getEventLast();

         ?>
        <tr class="second" code="<?php echo $key ?>">
            <td class="select"><?php if(isset($Correios->_conf['status']['completed'])) echo 'Completo'; else echo 'Em andamento'; ?></td>
            <td class="select"><?php if(!is_null($Evento)) echo $Evento->getLabel(); else echo 'Codigo inválido' ?></td>
            <td class="select"><?php if(!is_null($Evento)) echo $Evento->getLocation() ?></td>
            <td class="select"><?php if(!is_null($Evento)) echo $Evento->getDate().' as '.$Evento->getHour() ?></td>
            <td class="opcoes"><i class="fa fa-trash"></i> / <a target="_blank" href="<?php echo INCLUDE_PATH.'send.php?type=email&code='.$key ?>"><i class="fas fa-envelope"></i></a> / <a target="_blank" href="<?php echo INCLUDE_PATH.'send.php?type=pdf&code='.$key ?>"><i class="fas fa-file-pdf"></i></a></td>
        </tr>
        <?php } ?>
    </table>
    <div class="clear"></div>
</div>













<?php }else{ 
    if(strlen($_GET['produto']) == 13){
        $Correios = new \Baru\Correios\RastreioParser();
        $Correios->setCode($_GET['produto']);

        //relogio: OL588298165BR

        // Último evento
        $Evento = $Correios->getEventLast();

        //Lista todos eventos
        $Eventos = $Correios->getEventsList();

        // echo '<pre>';
        // print_r($Eventos);
        // echo '</pre>';

        if(strpos($Evento->getLabel(), 'postado') !== false) {
            $situation = 'postado';
        }else if(strpos($Evento->getLabel(), 'trânsito') !== false) {
            $situation = 'encaminhado';
        }else if(strpos($Evento->getLabel(), 'entrega') !== false) {
            $situation = 'entrega';
        }else if(strpos($Evento->getLabel(), 'entregue') !== false) {
            $situation = 'entregue';
        }

 ?>


<div class="container" style="max-width: 960px">
    <h2 class="title-ras">Informações do Rastreamento</h2>

    <div class="progresso-content">
       <i class="fas fa-boxes animate__animated ux1"><p>Postado</p></i>
       <i class="fas fa-shipping-fast animate__animated ux2"><p>A Caminho</p></i>
        <i class="fas fa-dolly animate__animated ux3"><p>Saiu para Entrega</p></i>
        <i class="fas fa-clipboard-check animate__animated ux4"><p>Entregue</p></i>
        <div situation="<?php echo $situation ?>" class="statusbar">
            <div class="progresso">
                <p></p>
            </div>
        </div>
    </div>
    <div class="wraper-table">
    <div class="wraper-content">
    <div class="content-info">
        <div class="left-info">
            <h3>Situação</h3>
            <h3>Local</h3>
            <div class="clear"></div>
        </div>
    </div>
    <div class="content-situation">

        <?php foreach ($Eventos as $key => $value){ 
                if(strpos($value->label, 'postado') !== false) $icon = 'fa-boxes';
                else if(strpos($value->label, 'saiu') !== false) $icon = 'fa-dolly';
                else if(strpos($value->label, 'trânsito') !== false) $icon = 'fa-shipping-fast';
                else if(strpos($value->label, 'entregue') !== false) $icon = 'fa-clipboard-check';
                
         ?>
        <div class="situation-info">
            <div class="Ppart">
                <p class="label-product"><i style="color: #e5005b" class="fas <?php echo $icon ?>"></i> <?php echo $value->label ?></p>
                <p class="hour-date"><?php echo $value->date.' as '.$value->hour ?></p>
            </div>
            <div class="Ppart">
                <p class="locale-product"><?php if($value->description != '') echo $value->description; else echo $value->location ?></p>
            </div>
        </div>
        <?php }  ?>
        <div class="clear"></div>
    </div>
    </div>
    </div>
</div>
















<?php }else{ echo '<script>location.href="'.INCLUDE_PATH.'"</script>'; }} ob_end_flush(); ?>
</div>
<script src="https://kit.fontawesome.com/79f4e40136.js" crossorigin="anonymous"></script>
<script src="<?php echo INCLUDE_PATH ?>jquery.js"></script>
<script src="<?php echo INCLUDE_PATH ?>functions.js"></script>
</body>
</html>