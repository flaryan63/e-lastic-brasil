<?php 
	if(isset($_GET['type']) && $_GET['type'] == 'pdf'){

		ob_start();
		session_start();
		$_SESSION['code'] = $_GET['code'];
		require('vendor/autoload.php');
		include('template2.php');
		$conteudo = ob_get_contents();
		ob_end_clean();
		
		$pdf = new \Mpdf\Mpdf();
		$pdf->SetAuthor('Flaviano Rodrigues');
		$pdf->SetCreator('Flaviano Rodrigues');
		
		$pdf->WriteHTML($conteudo);
		$pdf->Output();

	}else if(isset($_GET['type']) && $_GET['type'] == 'email'){

		if(isset($_GET['code']) && strlen($_GET['code']) == 13){
			ob_start();
			session_start();
			$_SESSION['code'] = $_GET['code'];
			require('vendor/autoload.php');
	        include('Email.php');
			include('template.php');
	        $mensagem = ob_get_contents();
			ob_end_clean();


			$Correios = new \Baru\Correios\RastreioParser();
	        $Correios->setCode($_GET['code']);
	        $Evento = $Correios->getEventLast();

	         if(strpos($Evento->getLabel(), 'postado') !== false) {
	            $situation = 'Postado !';
	        }else if(strpos($Evento->getLabel(), 'trânsito') !== false) {
	            $situation = 'Encaminhado';
	        }else if(strpos($Evento->getLabel(), 'entrega') !== false) {
	            $situation = 'Saiu para entrega !';
	        }else if(strpos($Evento->getLabel(), 'entregue') !== false) {
	            $situation = 'Entregue !';
	        }


	        $mail = new \Email('flamecursos.com','contato@flamecursos.com','Nasci2003!','Flame Cursos');
	        $mail->addAddress('joao.macedo@elastic.fit','João');
	        $tituloDoEmail = $situation;
	        $mail->formatarEmail(['assunto'=>$tituloDoEmail,'corpo'=>$mensagem]);
	        $mail->enviarEmail();
	        echo '<script>alert("E-mail enviado com exito !");</script>';
	        echo "<script>window.close();</script>";
		}else{
			die('Codigo inválido, volte a pagina anterior e tente novamente mais tarde.');
		}

	}else{
		die('Erro, volte a pagina anterior e tente novamente mais tarde.');
	}
 ?>