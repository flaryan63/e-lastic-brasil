<?php 
    require 'vendor/autoload.php';
    $Correios = new \Baru\Correios\RastreioParser();
    $Correios->setCode($_SESSION['code']);

    //relogio: OL588298165BR

    // Último evento
    $Evento = $Correios->getEventLast();

    //Lista todos eventos
    $Eventos = $Correios->getEventsList();
 ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <title>E-lastic Brasil</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0"/>
   <style type="text/css">
        div, p, a, li, td { -webkit-text-size-adjust:none; }
   </style>
</head>
<body style="margin:0;padding:0;font-family: helvetica;" bgcolor="#fff">
    <table width="600" align="center" cellpadding="0" cellspacing="0"> 
        <tr>
            <td style="padding: 0px 0 0px 0" bgcolor="#e5005b" align="center">
                <p style="color: #fff;font-size: 25px;">E-lastic Brasil</p>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 20px 20px 20px;">
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td style="border-bottom: 2px solid #c2c3c2;padding: 30px 0 2px 0"> 
                            <span style="color: #727172;font-size: 27px;"><?php echo $Evento->getLabel(); ?> !</span>
                            <p style="color: #727172;font-size: 16px;padding: 30px 0px 0px 0px">
                               Deseja mais informações sobre o envio do seu produto ? Ultilize o código de rastreamento <span style="color:#e5005b "><?php echo @$_SESSION['code']; ?></span>.
                            </p>
                            <p style="color: #727172;font-size: 16px;padding: 30px 0px 0px 0px">
                               Se preferir, visualize em <a target="_blank" href="http://flaviano.store/trabalho/send.php?type=pdf&code=<?php @echo $_SESSION['code'] ?>">PDF</a>.
                            </p>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 20px 20px 20px;">
                <table bgcolor="" width="100%" cellpadding="0" cellspacing="0">
                    <?php foreach ($Eventos as $key => $value) { 
                        ?>
                    <tr>
                        <td width="240" style="border-bottom: 2px solid #c2c3c2;">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="color: #e5005b;font-size: 25px;">
                                        <?php echo $value->label ?>
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
                                <tr>
                                    <td align="center" style="color: #727172;font-weight: bold;">
                                        
                                        <?php echo $value->date.' as '.$value->hour ?> 
                                    </td>
                                </tr>
                                <tr>
                                    <td style="padding: 10px 0 10px 0;color: #727172;font-size: 16px; line-height: 20px;">
                                        <p style="text-align: center;">
                                            <?php echo $value->description ?>
                                        </p>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="center" style="color: #727172;font-size: 14px;">
                                        <?php echo $value->location ?>
                                    </td>
                                </tr>
                                <tr><td height="10"></td></tr>
                            </table>
                        </td>
                        <tr><td height="30"></td></tr>
                    </tr>
                    <?php } ?>
                </table>
            </td>
        </tr>
        <tr>
            <td style="padding: 20px 20px 20px 20px;">
                <table bgcolor="" width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="240" style="">
                            <table width="100%" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td align="center" style="color: #e5005b;font-size: 25px;">
                                        <p>Dados do envio:</p>
                                    </td>
                                </tr>
                                <tr><td height="20"></td></tr>
                                <tr>
                                    <td style="color:#727172 ">
                                        <span style="font-weight: bold;">Nome:</span> <span>Flaviano Rodrigues Dos Santos</span>
                                        <br />
                                        <span style="font-weight: bold;">Tel:</span> <span>(22) 9 9216-4189</span>
                                        <br />
                                        <span style="font-weight: bold;">Endereço:</span> <span>Rua sheldon Outeiro - Araruama / Rio de janeiro</span>
                                    </td>
                                </tr>
                                 <tr><td height="50"></td></tr>
                                 <tr>
                                    <td align="left" style="color: #727172;font-size: 15px;font-weight: bold;">
                                        <span>Quase lá !</span>
                                        <br />
                                        <span style="padding-top: 10px">Equipe da E-lastic Brasil</span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <!-- <tr><td height="20"></td></tr> -->
                    </tr>
                </table>
            </td>
        </tr>
        <tr> 
            <td height="10"></td>
        </tr>
        <tr>
            <td style="padding: 10px 15px 0px 15px" bgcolor="#e5005b">
            <table width="100%" cellpadding="0" cellspacing="0">
                <tr style="color: #fff;font-size: 14px">
                    <td width="400" align="left">
                        <p>Todos os direitos reservados</p>
                    </td>
                    <td width="100" align="right">
                        <a style="color: #fff" href="">Sobre nós</a>
                    </td>
                    <td width="90" align="right">
                        <a style="color: #fff" href="">Nosso site</a>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>

<script src="https://kit.fontawesome.com/79f4e40136.js" crossorigin="anonymous"></script>
</body>
</html>